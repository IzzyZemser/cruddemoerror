import fs from 'fs/promises'
import path, { dirname }      from 'path';
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));
import {BadRequest} from '../utils/errors.mjs'

export const logRequests  = (req,res,next)=> {
    fs.appendFile('requestsHistory.txt', `${req.method} ${req.originalUrl} \n`, function (err) {
        if (err) throw err;
        console.log(err);
      });
      next()
}

export const checkDataMiddleware  = async (req,res,next)=> {
    try {
        const USERS_MOCK =  await fs.readFile(path.resolve(__dirname,'../data/mocks/MOCK_USERSs.json'),'utf-8');
        const {users} = JSON.parse(USERS_MOCK)
        req.db = users
    next()
      } catch (err) {
        console.log(err)
        next(err)
      }
    
}