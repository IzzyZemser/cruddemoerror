import express from 'express';
import bodyParser from 'body-parser';
import morgan 	  from 'morgan';
// import USERS_MOCK from './data/mocks/MOCK_USERS.json'

import usersRouter from './routes/userRouter.mjs'


const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(morgan('dev'))

app.use("/users", usersRouter);

app.get('/', (req, res) => {
    res.send('NOT FOUND')
  })


app.listen(3000, ()=> {
  console.log('Express server listening on port 3000')
})