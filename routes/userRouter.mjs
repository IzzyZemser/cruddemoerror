import express from 'express';
import fs from 'fs/promises'
import { v4 as uuidv4 }  from 'uuid'
import path, { dirname }      from 'path';
import { fileURLToPath } from 'url';
import {checkDataMiddleware, logRequests} from '../middleware/general.mjs'
import handleErrors from '../middleware/handlerrors.mjs'
import {BadRequest} from '../utils/errors.mjs'

const __dirname = dirname(fileURLToPath(import.meta.url));

let router = express.Router()

const writeToDB = async (db) => {
    try {
        await fs.writeFile(path.resolve(__dirname,'../data/mocks/MOCK_USERS.json'),  JSON.stringify({users: db}))
      } catch (err) {
        next(err)
      }
}


router.use(checkDataMiddleware);
router.use(logRequests);
router.use(handleErrors);


const getUserById = (db, id) => {
    return db.find((user) => user.id == id )
}
const addToDB = (db, user) => {
   db.push({...user, id:user.id ? user.id : uuidv4()});
   writeToDB(db)
}
const deleteUser = (db, id) => {
    const len = db.length;
    db = db.filter((user) => user.id != id )
    return len > db.length ? true : false;
}
const updateDB = (db, id, userData) => {
    const userIndx = db.findIndex((user) => user.id == id )
    console.log(userIndx)
    if(userIndx === -1){return false;}
    db[userIndx] = {...db[userIndx],...userData}
    writeToDB(db)
    return true;
}
const replaceUser = (db, id, userData) => {
    const userIndx = db.findIndex((user) => user.id == id )
    console.log(userIndx)
    if(userIndx === -1){return false;}
    db[userIndx] = {...userData, id: userData.id ?  userData.id : uuidv4()}
    writeToDB(db)
    return true;
}
router.get("/", (req, res) => {
    res.json(req.db)
})
router.get("/:id", (req, res) => {
    const {id} = req.params;
    res.json(getUserById(req.db, id))
})
router.post("/new", (req, res) => {
    if(req.body.first_name && req.body.last_name && req.body.email){
        addToDB(req.db, req.body)
        res.send("added User")
    }else{
        res.send("Fail, add the valid ibnput fields please")
    }
})
router.patch('/:id',(req, res) => {
    const {id} = req.params;
    const resFlag = updateDB(req.db, id, req.body);
    if(resFlag){
        res.status(200).send("User updated")
    } else{
        res.status(404).send("Fail, user with the given id wasn't found")
    }
});
router.put('/:id',(req, res) => {
    const {id} = req.params;
    const resFlag = replaceUser(req.db, id, req.body);
    if(resFlag){
        res.status(200).send("User updated")
    } else{
        res.status(404).send("Fail, user with the given id wasn't found")
    }
});
router.delete('/:id',(req, res) => {
    const {id} = req.params;
    const resFlag = deleteUser(req.db, id);
    if(resFlag){
        res.status(200).send("User deleted")
    } else{
        res.status(404).send("Fail, user with the given id wasn't found")
    }
});
router.get("*", (req,res) => {
    res.status(404).send("NOT FOUND")
})
export default router;
